from apps.qr_permata.models import ReportData
from django.urls import reverse
from rest_framework import serializers


class ReportDataSerializer(serializers.ModelSerializer):
	confirmed = serializers.SerializerMethodField()
	action = serializers.SerializerMethodField()
	trx_date = serializers.SerializerMethodField()
	update_by = serializers.SerializerMethodField()
	confirmed_at = serializers.SerializerMethodField()

	def get_confirmed_at(self, reportData):
		if reportData.confirmed_at is None:
			return "-"
		return reportData.confirmed_at.strftime("%d %B %Y %H:%M:%S")

	def get_trx_date(self, reportData):
		if reportData.trx_date is None:
			return "-"
		return reportData.trx_date.strftime("%d %B %Y %H:%M:%S")

	def get_update_by(self, reportData):
		if reportData.updated_by is None:
			return "-"
		return reportData.updated_by.username

	def get_confirmed(self, reportData):
		if reportData.is_confirmed:
			return "Confirmed"
		else:
			return "Unconfirmed"

	def get_action(self, reportData):
		return '<a href="'+ reverse('permata_view_notes', kwargs={'report_id':reportData.id}) +'"><i class="fa fa-fw fa-eye"></i></a>' + \
		'<a href="" data-val="'+ str(reportData.id) +'" data-toggle="modal" data-target="#notes-modal""><i class="fa fa-fw fa-edit"></i></a>'



	class Meta:
		model = ReportData
		fields = ('id', 'merchant_name', 'customer_name', 'pjsp_name', 'trx_id', 'trx_total', 'trx_date', 'confirmed', 'note', 'update_by', 'confirmed_at', 'action')
