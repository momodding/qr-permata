# Generated by Django 3.2.13 on 2022-07-10 06:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qr_permata', '0005_auto_20220710_1333'),
    ]

    operations = [
        migrations.AddField(
            model_name='reportdata',
            name='is_confirmed',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='reportdata',
            name='note',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
