# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import include, path, re_path
from apps.qr_permata import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'report-data', views.QrPermataViewSet, basename='report-data')

urlpatterns = [

    path('api/permata/', include(router.urls)),

    # The home page
    path('permata/setting/', views.setting, name='permata_setting'),
    path('permata/report/refresh/', views.refresh_data, name='permata_refresh'),
    path('permata/report/<int:report_id>/', views.view_notes, name='permata_view_notes'),
    path('permata/report/edit/<int:report_id>/', views.add_notes, name='permata_add_notes'),
    path('permata/report/edit/', views.edit_notes, name='permata_edit_notes'),
    path('permata/logout/', views.logout, name='permata_logout'),

    # Matches any html file
    # re_path(r'^.*\.*', views.pages, name='pages'),

]
