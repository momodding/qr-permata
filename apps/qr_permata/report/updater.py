from apps.qr_permata import browser_scrapper
from apps.qr_permata import scrapper
from apps.qr_permata.models import ReportData, SessionData


def get_and_save_report():
	session_data = SessionData.objects.get(key="PHPSESSID")
	try:
		scrapper_params = {'is_showmenu': False, 'session_id': session_data.session}
		report_data = scrapper.QrPermata(scrapper_params).get_all_report()
		if report_data is None:
			report_data = []
			session_data.is_active = False
			session_data.save()

		for report_data in report_data:
			if ReportData.objects.filter(trx_id=report_data[11]).exists() == False:
				ReportData.objects.create(
					mid=report_data[1],
					merchant_name=report_data[2],
					qr_type=report_data[3],
					trx_date=report_data[4],
					trx_type=report_data[5],
					status=report_data[6],
					customer_pan=report_data[7],
					customer_name=report_data[8],
					pjsp_name=report_data[9],
					payment_type=report_data[10],
					trx_id=report_data[11],
					bill_number=report_data[12],
					pay_reference=report_data[13],
					return_reference=report_data[14],
					trx_total=report_data[15],
					trx_return=report_data[16],
					mdr_total=report_data[17],
					service_fee=report_data[18],
					nett_total=report_data[19]
				)
	except Exception as e:
		session_data.is_active = False
		session_data.save()
		print(e)
	pass