from datetime import datetime

from apps.qr_permata import scrapper
from apps.qr_permata.forms import (UpdateCredential, UpdateReportData,
                                   ViewReportData)
from apps.qr_permata.models import ReportData, SessionData
from apps.qr_permata.serializers import ReportDataSerializer
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Q
from django.forms import model_to_dict
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from django_filters import CharFilter, NumberFilter, fields, filters, widgets
from rest_framework import viewsets
from rest_framework_datatables.django_filters.backends import \
    DatatablesFilterBackend
from rest_framework_datatables.django_filters.filters import GlobalFilter
from rest_framework_datatables.django_filters.filterset import \
    DatatablesFilterSet

from .report import updater

# Create your views here.

@login_required(login_url="/login/")
def refresh_data(request):
    updater.get_and_save_report()
    return redirect('home')

@login_required(login_url="/login/")
def setting(request):
    form = UpdateCredential(initial={'session': '', 'key': 'PHPSESSID'})
    try:
        sessionData = SessionData.objects.get(key="PHPSESSID")
        form = UpdateCredential(initial={'session': sessionData.session, 'key': sessionData.key})
    except SessionData.DoesNotExist:
        sessionData = None

    if request.method == "POST":
        if sessionData is not None:
            form = UpdateCredential(request.POST, instance=sessionData)
        else:
            form = UpdateCredential(request.POST)

        if form.is_valid():
            session = form.save(commit=False)
            session.is_active = True
            session.save()

            return redirect('home')

    context = {
        'segment': 'permata-setting',
        'form': form,
        'msg': form.errors,
    }

    html_template = loader.get_template('home/setting/_index.html')
    return HttpResponse(html_template.render(context, request))

@login_required(login_url="/login/")
def view_notes(request, report_id):
    report_data = None
    form = ViewReportData()
    if ReportData.objects.filter(id=report_id).exists():
        report_data = ReportData.objects.get(id=report_id)
        form = ViewReportData(initial=model_to_dict(report_data))

    context = {
        'segment': 'dashboard',
        'form': form,
        'msg': form.errors,
        'report_id': report_id,
    }

    html_template = loader.get_template('home/dashboard/_view.html')
    return HttpResponse(html_template.render(context, request))

@login_required(login_url="/login/")
def add_notes(request, report_id):
    report_data = None
    form = UpdateReportData()
    if ReportData.objects.filter(id=report_id).exists():
        report_data = ReportData.objects.get(id=report_id)
        form = UpdateReportData(initial=model_to_dict(report_data))

    if request.method == "POST":
        if report_data is not None:
            form = UpdateReportData(request.POST, instance=report_data)
        else:
            form = UpdateReportData(request.POST)

        if form.is_valid():
            report_data = form.save(commit=False)
            report_data.is_confirmed = True
            report_data.updated_by = request.user
            report_data.save()

            return redirect('home')

    context = {
        'segment': 'dashboard',
        'form': form,
        'msg': form.errors,
        'report_id': report_id,
    }

    html_template = loader.get_template('home/dashboard/_edit.html')
    return HttpResponse(html_template.render(context, request))

@login_required(login_url="/login/")
def edit_notes(request):
    if request.method == "POST":
        report_data = None
        report_id = request.POST.get('report_id')
        try:
            report_data = ReportData.objects.get(id=report_id)
        except ReportData.DoesNotExist:
            return redirect('home')

        report_data.note = request.POST.get('note')
        report_data.is_confirmed = True
        report_data.confirmed_at = datetime.now()
        report_data.updated_by = request.user
        report_data.save()

    return redirect('home')

@login_required(login_url="/login/")
def logout(request):
    try:
        session_data = SessionData.objects.get(key="PHPSESSID")
        scrapper_params = {'is_showmenu': False, 'session_id': session_data.session}
        scrapper.QrPermata(scrapper_params).logout()
        session_data.session = None
        session_data.is_active = False
        session_data.save()
    except SessionData.DoesNotExist:
        pass

    return redirect('home')


class GlobalCharFilter(GlobalFilter, filters.CharFilter):
    pass


class GlobalNumberFilter(GlobalFilter, filters.NumberFilter):
    pass

class ReportDataFilter(DatatablesFilterSet):

    customer_name = GlobalCharFilter(lookup_expr='icontains')
    merchant_name = GlobalCharFilter(lookup_expr='icontains')
    pjsp_name = GlobalCharFilter(lookup_expr='icontains')
    trx_id = GlobalCharFilter(lookup_expr='icontains')
    trx_total = GlobalCharFilter(lookup_expr='icontains')
    note = GlobalCharFilter(lookup_expr='icontains')
    trx_date = filters.CharFilter(method='filter_trx_date')
    confirmed = filters.CharFilter(method='filter_is_confirmed')

    class Meta:
        model = ReportData
        fields = ['customer_name', 'merchant_name', 'pjsp_name', 'trx_id', 'trx_total', 'note', 'confirmed']

    def filter_is_confirmed(self, queryset, name, value):
        confirmed = 0
        if value.casefold() == 'confirmed':
            confirmed = 1
        return queryset.filter(Q(is_confirmed__iexact=confirmed))

    def filter_trx_date(self, queryset, name, value):
        try:
            date_transaction = value.split('+')
            return queryset.filter(Q(trx_date__range=[date_transaction[0], date_transaction[1]]))
        except ValueError:
            return queryset

class QrPermataViewSet(viewsets.ModelViewSet):
    queryset = ReportData.objects.order_by('-trx_date')
    serializer_class = ReportDataSerializer
    filter_backends = [DatatablesFilterBackend]
    filterset_class = ReportDataFilter


