# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django import forms

from apps.qr_permata.models import ReportData, SessionData


class UpdateCredential(forms.ModelForm):
    session = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder": "Session ID",
                "class": "form-control"
            }
        ),
        required=False
    )
    key = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "placeholder": "Session Key",
                "class": "form-control"
            }
        )
    )

    class Meta:
        model = SessionData
        fields = ('session', 'key', 'is_active')


class UpdateReportData(forms.ModelForm):
    mid = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    merchant_name = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    qr_type = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    trx_date = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    trx_type = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    status = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    customer_pan = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    customer_name = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    pjsp_name = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    payment_type = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    trx_id = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    bill_number = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    pay_reference = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    return_reference = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    trx_total = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    trx_return = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    mdr_total = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    service_fee = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    nett_total = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    note = forms.CharField(
    widget=forms.Textarea(
        attrs={
            "placeholder": "Notes",
            "class": "form-control"
        }
    ))
    class Meta:
        model = ReportData
        fields = ('id', 'note', 'is_confirmed')

class ViewReportData(forms.ModelForm):
    mid = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    merchant_name = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    qr_type = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    trx_date = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    trx_type = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    status = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    customer_pan = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    customer_name = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    pjsp_name = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    payment_type = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    trx_id = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    bill_number = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    pay_reference = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    return_reference = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    trx_total = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    trx_return = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    mdr_total = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    service_fee = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    nett_total = forms.CharField(
    widget=forms.TextInput(
        attrs={
            "readonly": "",
            "class": "form-control"
        }
    ))
    note = forms.CharField(
    widget=forms.Textarea(
        attrs={
            "readonly": "",
            "placeholder": "Notes",
            "class": "form-control"
        }
    ))
    class Meta:
        model = ReportData
        fields = ('id', 'note', 'is_confirmed')