from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ReportData(models.Model):
	id = models.AutoField(primary_key=True)
	mid = models.CharField(null=True, blank=True, max_length=100)
	merchant_name = models.CharField(null=True, blank=True, max_length=100)
	qr_type = models.CharField(null=True, blank=True, max_length=100)
	trx_date = models.DateTimeField(null=True, blank=True)
	trx_type = models.CharField(null=True, blank=True, max_length=100)
	status = models.CharField(null=True, blank=True, max_length=100)
	customer_pan = models.CharField(null=True, blank=True, max_length=100)
	customer_name = models.CharField(null=True, blank=True, max_length=100)
	pjsp_name = models.CharField(null=True, blank=True, max_length=100)
	payment_type = models.CharField(null=True, blank=True, max_length=100)
	trx_id = models.CharField(null=True, blank=True, max_length=100)
	bill_number = models.CharField(null=True, blank=True, max_length=100)
	pay_reference = models.CharField(null=True, blank=True, max_length=100)
	return_reference = models.CharField(null=True, blank=True, max_length=100)
	trx_total = models.CharField(null=True, blank=True, max_length=100)
	trx_return = models.CharField(null=True, blank=True, max_length=100)
	mdr_total = models.CharField(null=True, blank=True, max_length=100)
	service_fee = models.CharField(null=True, blank=True, max_length=100)
	nett_total = models.CharField(null=True, blank=True, max_length=100)
	note = models.CharField(null=True, blank=True, max_length=100)
	is_confirmed = models.BooleanField(default=False)
	confirmed_at = models.DateTimeField(null=True, blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	updated_by = models.ForeignKey(
        User,
        models.CASCADE,
        verbose_name='User',
        related_name='report_data',
		blank=True, null=True
    )
	deleted_at = models.DateTimeField(null=True, blank=True)
	deleted = models.BooleanField(default=False)
	class Meta:
		db_table = 'report_data'
		managed = True
		verbose_name = 'Report Data'
		verbose_name_plural = 'Report Data'
		ordering = ['-trx_date']

class SessionData(models.Model):
	id = models.AutoField(primary_key=True)
	key = models.CharField(null=True, blank=True, max_length=100)
	session = models.CharField(null=True, blank=True, max_length=100)
	is_active = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	deleted_at = models.DateTimeField(null=True, blank=True)
	deleted = models.BooleanField(default=False)
	class Meta:
		db_table = 'session_data'
		managed = True
		verbose_name = 'Session Data'
		verbose_name_plural = 'Session Data'
		# get_latest_by = 'created_at'