import traceback
from bs4 import BeautifulSoup
import requests


class QrPermata(object):
    __base_url = 'https://qr.permatabank.com'

    def __init__(self, params=None):
        self.params = params
        self.setup()

    def setup(self):
        try:
            PHPSESSID = {
                'name': "PHPSESSID",
                'value': "1q2w3e4r"
            }
            if 'session_id' in self.params:
                PHPSESSID = {
                    'name': "PHPSESSID",
                    'value': self.params['session_id']
                }

            self.headers = {
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
                'Cache-Control': 'max-age=0',
                'Connection': 'keep-alive',
                'Cookie': 'PHPSESSID={};'.format(PHPSESSID['value']),
                'Referer': 'https://qr.permatabank.com/portal-merchant/login',
                'Sec-Fetch-Dest': 'document',
                'Sec-Fetch-Mode': 'navigate',
                'Sec-Fetch-Site': 'same-origin',
                'Sec-Fetch-User': '?1',
                'Sec-GPC': '1',
                'Upgrade-Insecure-Requests': '1',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.114 Safari/537.36',
            }

            if 'is_showmenu' in self.params and self.params['is_showmenu']:
                self.showMenu()
        except Exception as e:
            traceback.print_exc()
            print("Invalid browser setup")

    def showMenu(self):
        print("""
	       1. Cek Jumlah Pendapatan
	       2. Cek All Report
	       3. Logout
	       4. Exit/Quit
	       *************************************************************************
	       (Script ini masih dalam pengembangan, hanya bisa pilih 1 menu kemudian otomatis berhenti)
	       """)

        menu = input("Pilih Menu : ")
        if (menu == '1'):
            self.get_jumlah_pendapatan()
        elif (menu == '2'):
            self.get_all_report()
        elif (menu == '3'):
            self.logout()

        exit()

    def get_jumlah_pendapatan(self):
        try:
            url = self.__base_url + "/portal-merchant/account/dashboard"
            dashboard_page = requests.get(url=url, headers=self.headers)
            base_page = BeautifulSoup(
                dashboard_page.text, "html.parser")

            jumlah_pendapatan = base_page.find('h3', {'id': 'amount_success'}).text
            print(jumlah_pendapatan)

            return jumlah_pendapatan
        except Exception:
            print("Session timeout. please login again")
            traceback.print_exc()
            return None

    def get_all_report(self):
        try:
            url_path = "/portal-merchant/transaction/report/index"
            url = self.__base_url + url_path
            report_page = requests.get(url=url, headers=self.headers, timeout=120)
            base_page = BeautifulSoup(
                report_page.text, "html.parser")
            mutasi_data = []

            table_element_class = 'table-bordered overflow-auto table table-striped table-sm table-responsive'

            table_element = base_page.find('table', {'class': table_element_class})
            paged_mutasi_data = self.extract_table(table_element)
            mutasi_data.extend(paged_mutasi_data)

            pagination = base_page.find('ul', {'class': 'pagination'})
            if pagination is not None:
                next_button = pagination.find("li", {"class": "next"})
                is_disabled = pagination.find("li", {"class": "next disabled"})
                while next_button is not None and is_disabled is None:
                    next_page_value = next_button.find("a").get("data-page")
                    url = self.__base_url + url_path + "?per-page=15&page=" + str(int(next_page_value)+1)
                    paged_report_page = requests.get(url=url, headers=self.headers, timeout=120)
                    parsed_report_page = BeautifulSoup(paged_report_page.text, "html.parser")

                    table_element = parsed_report_page.find('table', {'class': table_element_class})
                    if table_element is not None:
                        paged_mutasi_data = self.extract_table(table_element)
                        mutasi_data.extend(paged_mutasi_data)

                    new_pagination = parsed_report_page.find('ul', {'class': 'pagination'})
                    next_button = new_pagination.find("li", {"class": "next"})
                    is_disabled = new_pagination.find("li", {"class": "next disabled"})

            return mutasi_data
        except Exception:
            print("Session timeout. please login again")
            traceback.print_exc()
            return None

    def extract_table(self, table_report):
        table_body = table_report.find("tbody")
        table_row = table_body.find_all("tr")
        mutasi_data = []
        for row in table_row:
            if not row.has_attr('data-key'):
                continue
            table_data = row.find_all("td")
            raw_data = []
            for data in table_data:
                raw_data.append(data.text)

            print("%s \t%s \t\t\t\t%s \t\t\t\t%s" %
                  (raw_data[1], raw_data[2], raw_data[3], raw_data[4]))
            print("\n")
            mutasi_data.append(raw_data)
        return mutasi_data

    def logout(self):
        try:
            url = self.__base_url + "/portal-merchant/logout"
            requests.get(url=url, headers=self.headers)
            print("Logout success")
        except Exception:
            print("Session timeout. please login again")
            traceback.print_exc()
            return None


if __name__ == "__main__":
    scrapper_params = {'is_showmenu': True,
                       'session_id': "1q2w3e4r"}
    QrPermata(scrapper_params)
