import time
from unicodedata import name
import urllib
import getpass
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import Select
from bs4 import BeautifulSoup
from selenium.webdriver.chrome.service import Service


class QrPermata(object):
    __base_url = 'https://qr.permatabank.com'

    def __init__(self, params=None):
        self.params = params
        self.setup()

    def setup(self):
        try:
            PHPSESSID = {
                'name': "PHPSESSID",
                'value': "1q2w3e4r5t6y7u8i"
            }
            if 'session_id' in self.params:
                PHPSESSID = {
                    'name': "PHPSESSID",
                    'value': self.params['session_id']
                }

            self.__option = webdriver.ChromeOptions()
            self.__option.add_argument("--incognito")
            # change into your browser path
            # self.__option.binary_location = r'C:\\Program Files\\BraveSoftware\\Brave-Browser\\Application\\brave.exe'
            # change into chrome driver path
            # exec_path = "C:\\Users\\udzel\\Documents\\Python\\qr_permata_web\\core\\chromedriver.exe"
            exec_path = "/usr/bin/chromedriver"

            if 'is_showmenu' in self.params and self.params['is_showmenu'] == False:
                self.__option.add_argument("headless")
            self.__driver = webdriver.Chrome(
                service=Service(exec_path), options=self.__option)
            self.__driver.wait = WebDriverWait(self.__driver, 10)

            self.__driver.get(self.__base_url + "/portal-merchant/")
            self.__driver.add_cookie(PHPSESSID)
            if 'is_showmenu' in self.params and self.params['is_showmenu']:
                self.showMenu()
        except Exception as e:
            print("Invalid browser setup")

    def showMenu(self):
        print("""
	       1. Cek Jumlah Pendapatan
	       2. Cek All Report
	       3. Exit/Quit
	       *************************************************************************
	       (Script ini masih dalam pengembangan, hanya bisa pilih 1 menu kemudian otomatis berhenti)
	       """)

        menu = input("Pilih Menu : ")
        if (menu == '1'):
            self.get_jumlah_pendapatan()
        elif (menu == '2'):
            self.get_all_report()

        self.__driver.quit()
        exit()

    def get_jumlah_pendapatan(self):
        try:
            self.__driver.get(self.__base_url +
                              "/portal-merchant/account/dashboard")
            jumlah_pendapatan_page = self.__driver.wait.until(
                EC.presence_of_element_located((By.XPATH, "//h3[@id=\"amount_success\"]")))
            jumlah_pendapatan = BeautifulSoup(
                jumlah_pendapatan_page.get_attribute('innerHTML'), "html.parser")

            print(jumlah_pendapatan.text)
            return jumlah_pendapatan.text
        except TimeoutException:
            print("Session timeout. please login again")
            return None

    def get_all_report(self):
        try:
            self.__driver.get(self.__base_url +
                              "/portal-merchant/transaction/report/index")
            menu_report = self.__driver.wait.until(EC.presence_of_element_located(
                (By.XPATH, "//a[@href=\"/portal-merchant/transaction/report/index\"]")))
            menu_report.click()

            mutasi_data = []
            if len(self.__driver.find_elements(By.XPATH, "//ul[@class=\"pagination\"]")) > 0:
                pagination = self.__driver.wait.until(EC.presence_of_element_located(
                    (By.XPATH, "//ul[@class=\"pagination\"]")))

                pagination_list = BeautifulSoup(
                    pagination.get_attribute('outerHTML'), "html.parser")

                page_list = []
                for p in pagination_list.find_all("li"):
                    if p.has_attr('class') and p.get('class')[0] in ['first', 'prev', 'next', 'last']:
                        continue
                    else:
                        page_list.append(p.find('a').text)

                for page in page_list:
                    self.__driver.get(
                        self.__base_url + "/portal-merchant/transaction/report/index?per-page=15&page=" + page)
                    if len(self.__driver.find_elements(By.XPATH, "//table[@class=\"table-bordered overflow-auto table table-striped table-sm table-responsive\"]")) > 0:
                        paged_mutasi_data = self.extract_table(
                            "//table[@class=\"table-bordered overflow-auto table table-striped table-sm table-responsive\"]")
                        mutasi_data.extend(paged_mutasi_data)
            else:
                paged_mutasi_data = self.extract_table(
                    "//table[@class=\"table-bordered overflow-auto table table-striped table-sm table-responsive\"]")
                mutasi_data.extend(paged_mutasi_data)

            return mutasi_data
        except TimeoutException:
            print("Session timeout. please login again")
            return None

    def extract_table(self, path_element):
        table_report_page = self.__driver.wait.until(EC.presence_of_element_located(
            (By.XPATH, path_element)))
        table_report = BeautifulSoup(
            table_report_page.get_attribute('outerHTML'), "html.parser")

        table_body = table_report.find("tbody")
        table_row = table_body.find_all("tr")
        mutasi_data = []
        for row in table_row:
            if not row.has_attr('data-key'):
                continue
            table_data = row.find_all("td")
            raw_data = []
            for data in table_data:
                raw_data.append(data.text)

            print("%s \t%s \t\t\t\t%s \t\t\t\t%s" %
                  (raw_data[1], raw_data[2], raw_data[3], raw_data[4]))
            print("\n")
            mutasi_data.append(raw_data)
        return mutasi_data


if __name__ == "__main__":
    scrapper_params = {'is_showmenu': True,
                       'session_id': "knfbn2iiq85sg78a2br3jb9cm1"}
    QrPermata(scrapper_params)
