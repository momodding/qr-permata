# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from apps.home.forms import CreateUserForm, UpdateUserForm
from apps.qr_permata import browser_scrapper, scrapper
from apps.qr_permata.models import ReportData, SessionData
from django import template
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Count
from django.db.models.functions import TruncDate
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.template import loader
from django.urls import reverse


@login_required(login_url="/login/")
def index(request):
    try:
        session_data = SessionData.objects.get(key="PHPSESSID")
    except SessionData.DoesNotExist:
        return redirect('permata_setting')

    scrapper_params = {'is_showmenu': False, 'session_id': session_data.session}
    jumlah_pendapatan = scrapper.QrPermata(scrapper_params).get_jumlah_pendapatan()
    if jumlah_pendapatan is None:
        jumlah_pendapatan = "Rp. 0.0"
        session_data.is_active = False
        session_data.save()

    merchant_name = ReportData.objects.all().values('merchant_name').order_by().distinct()

    context = {
        'segment': 'index',
        'jumlah_pendapatan': jumlah_pendapatan,
        'scrapper_status': session_data.is_active,
        'merchant_names': merchant_name
    }

    html_template = loader.get_template('home/dashboard/_index.html')
    return HttpResponse(html_template.render(context, request))

@login_required(login_url="/login/")
def user_management_index(request):
    users = User.objects.all()
    context = {
        'segment': 'user-management',
        'users': users
    }


    html_template = loader.get_template('home/user_management/_index.html')
    return HttpResponse(html_template.render(context, request))

@login_required(login_url="/login/")
def user_management_add(request):
    form = CreateUserForm()
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            raw_password = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=raw_password)

            return redirect('user_management')

    context = {
        'segment': 'user-management',
        'form': form,
        'msg': form.errors,
    }

    html_template = loader.get_template('home/user_management/_add.html')
    return HttpResponse(html_template.render(context, request))

@login_required(login_url="/login/")
def user_management_edit(request, user_id):
    users = User.objects.get(id=user_id)
    form = UpdateUserForm(
        initial={
            'username': users.username,
            'email': users.email
        }
    )
    if request.method == "POST":
        form = UpdateUserForm(request.POST)
        if form.is_valid():
            users.username = form.cleaned_data.get("username")
            users.email = form.cleaned_data.get("email")
            users.set_password(form.cleaned_data.get("password1"))
            users.save()

            return redirect('user_management')

    context = {
        'segment': 'user-management',
        'form': form,
        'msg': form.errors,
        'user_id': users.id
    }

    html_template = loader.get_template('home/user_management/_edit.html')
    return HttpResponse(html_template.render(context, request))

@login_required(login_url="/login/")
def user_management_delete(request, user_id):
    users = User.objects.get(id=user_id)
    if users is not None:
        users.delete()

    return redirect('user_management')


@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:

        load_template = request.path.split('/')[-1]

        if load_template == 'admin':
            return HttpResponseRedirect(reverse('admin:index'))
        context['segment'] = load_template

        html_template = loader.get_template('home/' + load_template)
        return HttpResponse(html_template.render(context, request))

    except template.TemplateDoesNotExist:

        html_template = loader.get_template('home/page-404.html')
        return HttpResponse(html_template.render(context, request))

    except:
        html_template = loader.get_template('home/page-500.html')
        return HttpResponse(html_template.render(context, request))
