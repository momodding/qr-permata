# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from apps.home import views

urlpatterns = [

    # The home page
    path('', views.index, name='home'),
    path('user-management/', views.user_management_index, name='user_management'),
    path('user-management/add/', views.user_management_add, name='user_management_add'),
    path('user-management/edit/<int:user_id>/', views.user_management_edit, name='user_management_edit'),
    path('user-management/delete/<int:user_id>/', views.user_management_delete, name='user_management_delete'),

    # Matches any html file
    # re_path(r'^.*\.*', views.pages, name='pages'),

]
